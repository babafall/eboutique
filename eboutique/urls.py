"""eboutique URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from backoffice.views import *

# from django.conf.urls import patterns, include, url

#from eboutique.views import *

from django.contrib.auth.decorators import login_required


urlpatterns = [
    path('admin/', admin.site.urls),
    # path('', views.home, name='home')
    path('', LoginView.as_view()),
    
    path('backoffice/', login_required(TemplateView.as_view(template_name='backoffice/index.html'))),
    
    path('logout/', LogoutView.as_view()),

    path('client/new/', client_new, name='client_new'),
    
    path('client/', client, name='client'),


]
