# -*- coding: utf-8 -*-

from django import forms
from backoffice.models import *


class ClientForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = '__all__'