from django.shortcuts import render
from django.http import *

from django.contrib.auth import authenticate, login, logout
from django.views.generic import TemplateView
from django.conf import settings

from backoffice.forms import *
from backoffice.models import Client


# Create your views here.


def home(request):
    return HttpResponse("Bonjour monde!")


class LoginView(TemplateView):
	template_name = 'front/index.html'
	def post(self, request, **kwargs):
		username = request.POST.get('username', False)
		password = request.POST.get('password', False)
		user = authenticate(username=username, password=password)
		if user is not None and user.is_active:
			login(request, user)
			return HttpResponseRedirect( settings.LOGIN_REDIRECT_URL )
		return render(request, self.template_name)

class LogoutView(TemplateView):
	template_name = 'front/index.html'
	def get(self, request, **kwargs):
		logout(request)
		return render(request, self.template_name)


def client_new(request):
    # Construire le formulaire, soit avec les données postées,
    # soit vide si l'utilisateur accède pour la première fois
    # à la page.
    form = ClientForm(request.POST or None)
    # Nous vérifions que les données envoyées sont valides
    # Cette méthode renvoie False s'il n'y a pas de données 
    # dans le formulaire ou qu'il contient des erreurs.
    if form.is_valid(): 
        # Ici nous pouvons traiter les données du formulaire
        # sujet = form.cleaned_data['sujet']
        # message = form.cleaned_data['message']
        # envoyeur = form.cleaned_data['envoyeur']
        # renvoi = form.cleaned_data['renvoi']

        form.save()
        # Nous pourrions ici envoyer l'e-mail grâce aux données 
        # que nous venons de récupérer
        envoi = True
        return HttpResponseRedirect( '/client/' )

    # Quoiqu'il arrive, on affiche la page du formulaire.
    return render(request, 'backoffice/client_new.html', locals())


def client(request):
	clients = Client.objects.all().order_by('firstName').order_by('lastName').values()
	return render(request, 'backoffice/client.html', locals())