# -*- coding: utf-8 -*-

from django.db import models
from django.core.exceptions import ValidationError
from django.utils import timezone

# Create your models here.


class Product(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nom produit")
    code = models.IntegerField(verbose_name="Code Produit")
    price_ht   = models.DecimalField(default=0.00, max_digits=10, decimal_places=2, verbose_name="Prix unitaire HT")
    price_ttc  = models.DecimalField(default=0.00, max_digits=10, decimal_places=2, verbose_name="Prix unitaire TTC")
    def __str__(self):
        return "{0} [{1}]".format(self.name, self.code)


class ProductItem(models.Model):
    color   = models.CharField(max_length=100, verbose_name="Couleur")
    product = models.ForeignKey('Product', on_delete=models.CASCADE, verbose_name="Produit")
    code    = models.IntegerField(verbose_name="Code Article")
    picture = models.ImageField(verbose_name="Photo", null=True)
    descriptionn = models.TextField(verbose_name="Description", null=True)
    def __str__(self):
        return "{0} {{1}} [{2}]".format(self.product.name, self.color, self.code)


class Client(models.Model):
    def validate_email(value):
        if not "@gmail.com" in value:
            raise ValidationError("Un contact gmail doit etre tape.")
        else:
            return value
    firstName = models.CharField(max_length=150, verbose_name="Prenom")
    lastName = models.CharField(max_length=150, verbose_name="Nom")
    cniNumber = models.CharField(max_length=13, verbose_name="Numero identification", unique=True)
    dateOfBirth = models.DateField(verbose_name="Date naissance")
    placeOfBirth = models.CharField(max_length=150, verbose_name="Lieu naissance")
    phone = models.CharField(max_length=15, verbose_name="Telephone")
    email = models.EmailField(verbose_name="Email", validators=[validate_email])
    def __str__(self):
        return "{0} {1}, ({2} à {3})".format(self.firstName, self.lastName, self.dateOfBirth, self.placeOfBirth)



class Command(models.Model):
    montant = models.DecimalField(max_digits=10, decimal_places=2)
    productLines = models.ManyToManyField(ProductItem, through='CommandLine')
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    code = models.IntegerField(verbose_name="Code commande")
    def __str__(self):
        return "{0}, montant = {1}".format(self.code, self.montant)


class CommandLine(models.Model):
    command = models.ForeignKey(Command, on_delete=models.CASCADE)
    productItem = models.ForeignKey(ProductItem, on_delete=models.CASCADE)
    dateCommand = models.DateTimeField(default=timezone.now)
    quantity = models.IntegerField()
